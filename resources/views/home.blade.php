@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="content">
            <div class="title">
                Search on GitHub repositories
            </div>

            <form method="POST" action="{{ route('search') }}">
                <input type="text" size=60 value="{{ $searchRequest }}" name="q">
                <button type="submit">Search</button>
                @csrf
            </form>
        </div>
        
        @if(isset($itemsTotalCount))
            <hr>
            <p>Found {{ $itemsTotalCount }} items.</p>

            @if($itemsTotalCount)
                {{ $entries->links() }}

                <ul>
                    @foreach($entries->all() as $item)
                        <li class="repo-item">
                            <div>
                                <b>{{ $item->name }}</b>
                                | Author: <span style="text-decoration: underline;">{{ $item->owner->login }}</span>
                                | Stars: {{ $item->stargazers_count }}
                                |
                                @php
                                    $favData = array(
                                      'repo_id' => $item->id,
                                      'name' => $item->name,
                                      'owner_login' => $item->owner->login,
                                      'html_url' => $item->html_url,
                                      'description' => htmlspecialchars($item->description),
                                      'stargazers_count' => $item->stargazers_count
                                    );
                                    $favDataJson = json_encode($favData);
                                @endphp
                                @if (!$favorites->contains('repo_id', $item->id))
                                    <button class="btn btn-primary btn-sm"
                                            onclick="javascript:addFavorites( this,
                                                    '{{ $favDataJson }}'
                                                    )">
                                        Add to favorites
                                    </button>
                                @else
                                    <button class="btn btn-primary btn-sm"
                                            onclick="javascript:removeFavorites( this,
                                                    '{{ $favorites->where('repo_id', $item->id)->first()->id }}',
                                                    false,
                                                    '{{ $favDataJson }}'
                                                    )">
                                        Remove from favorites
                                    </button>
                                @endif

                            </div>
                            <div><a href="{{ $item->html_url }}" target="_blank">{{ $item->html_url }}</a></div>
                            @if(!empty($item->description))
                                <div style="color: #888;">
                                    Description: {{ $item->description }}
                                </div>
                            @endif

                        </li>
                    @endforeach
                </ul>

                {{ $entries->links() }}
            @endif
        @endif

    </div>
@endsection

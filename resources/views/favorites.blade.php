@extends('layouts.app')

@section('content')
    <div class="container">

        <h2>Favorites</h2>

        <hr>

        @isset($items)
            <ul>
                @foreach($items as $item)
                    <li id="item-{{ $item->id }}" class="repo-item">
                        <div>
                            <b>{{ $item->name }}</b>
                            | Author: <span style="text-decoration: underline;">{{ $item->owner_login }}</span>
                            | Stars: {{ $item->stargazers_count }}
                            |
                            <button class="btn btn-primary btn-sm"
                                    onclick="javascript:removeFavorites( this,
                                            '{{ $item->id }}',
                                            true
                                            )">
                                Remove from favorites
                            </button>
                        </div>
                        <div><a href="{{ $item->html_url}}" target="_blank">{{ $item->html_url}}</a></div>
                        @if(!empty($item->description))
                            <div style="color: #888;">
                                Description: {{ htmlspecialchars_decode($item->description) }}
                            </div>
                        @endif

                    </li>
                @endforeach
            </ul>
        @endisset

    </div>
@endsection

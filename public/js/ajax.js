function addFavorites(btn, favData) {
    $.post('/favorites/add',
        {
            '_token': $('meta[name=csrf-token]').attr('content'),
            fav_data: favData
        }, function (data) {
            try {
                var res = $.parseJSON(JSON.stringify(data));
                if (res.success == 1) {
                    btn.innerHTML = 'Remove from favorites';
                    btn.onclick = function () {
                        removeFavorites(btn, res.last_id, false, favData);
                    }
                }
            } catch (err) {
                //
            }
        });
}

function removeFavorites(btn, fav_id, hideBtn, favData) {
    $.post('/favorites/remove',
        {
            '_token': $('meta[name=csrf-token]').attr('content'),
            id: fav_id
        }, function (data) {
            try {
                var res = $.parseJSON(JSON.stringify(data));
                if (res.success == 1) {
                    btn.innerHTML = 'Add to favorites';
                    btn.onclick = function () {
                        addFavorites(btn, favData);
                    }
                    if (hideBtn === true)
                        $("#item-" + fav_id).hide();
                }
            } catch (err) {
                //
            }
        });
}

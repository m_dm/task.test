<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\Favorites;

class FavoritesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $items = Favorites::all();

        return view('favorites', [
            'items' => $items
        ]);
    }

    public function addFavorites(Request $request)
    {
        $result = array('success' => 0, 'error' => '');
        $favData = json_decode($request->input('fav_data'));
        try {
            $favorite = new Favorites;
            $favorite->repo_id = $favData->repo_id;
            $favorite->name = $favData->name;
            $favorite->owner_login = $favData->owner_login;
            $favorite->html_url = $favData->html_url;
            if ($favData->description !== null) {
                $favorite->description = $favData->description;
            } else {
                $favorite->description = '';
            }
            $favorite->stargazers_count = $favData->stargazers_count;
            $res = $favorite->save();
            $result['success'] = ($res) ? 1 : 0;
            $result['last_id'] = $favorite->id;
        } catch (QueryException $e) {
            $result['error'] = $e->getMessage();
        }

        return response()->json($result);
    }

    public function removeFavorites(Request $request)
    {
        $result = array('success' => 0, 'error' => '');
        try {
            $favorite = Favorites::find($request->input('id'));
            if ($favorite) {
                $res = $favorite->delete();
                $result['success'] = ($res) ? 1 : 0;
            }
        } catch (QueryException $e) {
            $result['error'] = $e->getMessage();
        }

        return response()->json($result);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Favorites;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $q = $request->q;

        return view('home', [
            'searchRequest' => $q
        ]);
    }

    public function search(Request $request)
    {
        $q = $request->q;
        $page = isset($request->page) ? $request->page : 1; // Get the page=1 from the url
        $perPage = 30;

        $itemsTotalCount = 0;
        $items = array();
        $entries = array();

        if (!empty($q)) {
            $client = new \GuzzleHttp\Client();
            $res = $client->get("https://api.github.com/search/repositories?q={$q}&page={$page}&per_page={$perPage}", []);
            $searchRes = json_decode($res->getBody());
            $itemsTotalCount = $searchRes->total_count;
            $items = $searchRes->items;
        }

        if ($itemsTotalCount) {
            $entries = new LengthAwarePaginator(
                $items,
                ($itemsTotalCount > 1000) ? 1000 : $itemsTotalCount, //GitHub allow only first 1000 search results
                $perPage, // Items per page
                $page, // Current page
                ['path' => $request->url(), 'query' => ['q' => $q]]
            );
        }

        $favorites = Favorites::select(['id', 'repo_id'])->get();

        return view('home', [
            'searchRequest' => $q,
            'itemsTotalCount' => $itemsTotalCount,
            'entries' => $entries,
            'favorites' => $favorites
        ]);
    }
}
